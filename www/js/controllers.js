var app=angular.module('AvanApp.controllers', [])

app.controller('Init', function($scope,$state){
  $scope.global={};
  $scope.global.title="AvanApp";
  $scope.global.avanUsername="Anirban";
  $scope.global.avanStatus="I'm working";
  $scope.global.search=true;
  //$state.go('home');
  $state.transitionTo('home.news');
  $scope.searchExpand=function($scope){
    $scope.global.search=true;
    };
});
app.controller('trendingCtrl', function($scope,$state,$http){
  //$scope.global.title="Avan Trends";
  $state.popular=[];
  $http({
    method:"GET",
    url:"https://newsapi.org/v1/articles?source=the-times-of-india&sortBy=latest&apiKey=2971176f22b141a4b7e1bb7ee490b18e"
  }).then(function(newsData){
    $scope.popular=newsData.data
  })
  $scope.deRefreashTrending=function($state){
    $http({
        method:"GET",
        url:"https://newsapi.org/v1/articles?source=the-times-of-india&sortBy=latest&apiKey=2971176f22b141a4b7e1bb7ee490b18e"
      }).then(function(newsData){
        $scope.popular=newsData.data
      })
  };
});

app.controller('newsCtrl',function($scope,$state,$http){
  //$scope.global.title="Avan News";
  $state.news=[];
  $http({
    method:"GET",
    url:"https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=2971176f22b141a4b7e1bb7ee490b18e"
  }).then(function(newsData){
    //$scope.status=newsData.
    $scope.news=newsData.data
  });
  $scope.deRefreashNews=function($state){
    $http({
      method:"GET",
      url:"https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=2971176f22b141a4b7e1bb7ee490b18e"
    }).then(function(newsData){
      $scope.news=newsData.data
    });
  }
})

app.controller('profileCtrl', function($scope,$state,$http){
  //$scope.global.title="Profile";
});

app.controller('addStatusCtrl', function($scope,$state,$http){
  $scope.saveStatus=function(txtStatus){
    //update status
    //console.log(txtStatus)
    $scope.global.avanStatus=txtStatus;
    $state.transitionTo('profile');
  }
});

app.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  //console.log("jfhf");
  //$scope.global.title="Avan Chat";
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

app.controller('ChatDetailCtrl', function($scope, $stateParams, Chats,$ionicScrollDelegate) {
  $scope.chat = Chats.get($stateParams.chatId);
  $scope.msgs=[];
  $scope.msgs.push({ text: $scope.chat.lastText, id: 1});
  $scope.sendMsg=function(txtMsg){
    console.log(txtMsg)
    if(txtMsg===""){
    }
    else{
      $scope.msgs.push({ text:txtMsg, id: 2});
      $ionicScrollDelegate.scrollBottom()
    }
  }

})
app.controller('RoomCtrl', function($scope,ionicTimePicker,ionicDatePicker) {
  $scope.booking='none';
  /// Date picker
  $scope.currentDate = new Date();
  var datePickObj = {
        callback: function (val) {  //Mandatory
         //console.log('Return value from the datepicker popup is : ' + val, new Date(val));
          if (typeof (val) === 'undefined') {
            console.log('Time not selected');
          } else {
            $scope.currentDate = new Date(val);
            //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          }
        },
           disabledDates: [            //Optional
                   new Date(2016, 2, 16),
                   new Date(2015, 3, 16),
                   new Date(2015, 4, 16),
                   new Date(2015, 5, 16),
                   new Date('Wednesday, August 12, 2015'),
                   new Date("08-16-2016"),
                   new Date(1439676000000)
                 ],
                 from: new Date(2012, 1, 1), //Optional
                 to: new Date(2040, 10, 30), //Optional
                 inputDate: new Date(),      //Optional
                 mondayFirst: true,          //Optional
                 disableWeekdays: [0],       //Optional
                 closeOnSelect: false,       //Optional
                 templateType: 'popup'       //Optional
      };
  
  $scope.datePickerCallback = function () {
    ionicDatePicker.openDatePicker(datePickObj);
  };
  /// Time picker 
  $scope.selectedTime = new Date();
  var timePickObj = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          $scope.selectedTime = new Date(val * 1000);
          //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
        }
      },
      inputTime: 50400,   //Optional
      format: 24,         //Optional
      step: 15,           //Optional
      setLabel: 'Set'    //Optional
    };
    $scope.TimePicker=function(){
      ionicTimePicker.openTimePicker(timePickObj);
    }

  // check availability
  $scope.checkAval=function(){
    $scope.booking='ok'
  }
  $scope.bookRoom=function(){
    $scope.booking='booked'
  }
})
app.controller('EventCtrl', function($scope,date) {
  //$scope.global.title="Avan Events";
  $scope.onezoneDatepicker = {
      date: date, // MANDATORY                     
      mondayFirst: false,                
      months: months,                    
      daysOfTheWeek: daysOfTheWeek,     
      startDate: startDate,             
      endDate: endDate,                    
      disablePastDays: false,
      disableSwipe: false,
      disableWeekend: false,
      disableDates: disableDates,
      disableDaysOfWeek: disableDaysOfWeek,
      showDatepicker: false,
      showTodayButton: true,
      calendarMode: false,
      hideCancelButton: false,
      hideSetButton: false,
      highlights: highlights,
      callback: function(){
          $scope.selectedDate=$scope.onezoneDatepicker.date;
      }
    };
})
app.controller('LearnCtrl', function($scope,Videos) {
  //$scope.global.title="Avan Learn";
  $scope.videos = Videos.all();
})
app.controller('VideoCtrl', function($scope, $stateParams) {
  //$scope.global.title="Avan Learn";
  $scope.video = Videos.get($stateParams.Id);
})