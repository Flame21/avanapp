angular.module('avanapp.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Arunava Das',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Syed Hashir Sher',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})
.factory('Videos',function(){
  var videos=[{
    id: 0,
    name: 'Overview of fieldglass VMS-English Version',
    desc: 'Overview of fieldglass VMS-English Version',
    thumb: 'img/Vid1.jpg',
    video: 'img/video0.mp4'
  }, {
    id: 1,
    name: 'How to enable Digital Transformation On Cloud',
    desc: 'How to enable Digital Transformation On Cloud',
    thumb: 'img/Vid2.jpg',
    video: 'img/video1.mp4'
  }, {
    id: 2,
    name: 'SAP C4C Introduction',
    desc: 'SAP C4C Introduction',
    thumb: 'img/Vid3.jpg',
    video: 'img/video2.mp4'
  }, {
    id: 3,
    name: 'Realizing the potential of Intellegent digital business',
    desc: 'Realizing the potential of Intellegent digital business',
    thumb: 'img/Vid4.jpg',
    video: 'img/video3.mp4'
  }, {
    id: 4,
    name: 'What is Cloud Computing?',
    desc: 'What is Cloud Computing?',
    thumb: 'img/Vid5.jpg',
    video: 'img/video4.mp4'
  }]
  return {
    all: function() {
      return videos;
    },
    remove: function(video) {
      videos.splice(videos.indexOf(video), 1);
    },
    get: function(videoId) {
      for (var i = 0; i < videos.length; i++) {
        if (videos[i].id === parseInt(videoId)) {
          return videos[i];
        }
      }
      return null;
    }
  };
});
