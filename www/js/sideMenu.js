//
angular.module("avanapp.directives.sideMenu",['ionic'])
.directive('sideMenu',function(){
	return{
		restrict: 'E',
		scope:{
			expanded:'=',
			onIconClick:'=',
			onSearch:'=',
			onCancel:'=',
			title:'=',
			avanUsername:'=',
			avanStatus:'='
		},
		templateUrl:'template/SideMenu.html',
		controller: function($scope,$ionicFilterBar) {
			//alert($scope.expanded);
		}

	};

});