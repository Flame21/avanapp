// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app=angular.module('AvanApp', ['ionic',
  'avanapp.directives.headerbar',
  'jett.ionic.filter.bar',
  'avanapp.directives.sideMenu',
  'avanapp.services',
  'AvanApp.controllers',
  'ionic-datepicker',
  'ionic-timepicker'])

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


app.config(function($stateProvider, 
                    $urlRouterProvider,
                    ionicDatePickerProvider,
                    ionicTimePickerProvider) {
        $stateProvider
            .state('home', {
                url:'/home',
                abstract:true,
                  templateUrl:'/states/home.html',
                  //controller:'homeCtrl'
            })
            .state('profile', {
                url:'/profile',
                  templateUrl:'/states/profile.html',
                  controller:'profileCtrl'
            })
            .state('addStatus', {
                url:'/addStatus',
                  templateUrl:'/states/addStatus.html',
                  controller:'addStatusCtrl'
            })
            .state('home.news', {
              url: '/news',
              views:{
                'newsfeeds':{
                  templateUrl: '/states/newsFeeds.html',
                  controller: 'newsCtrl'
                }
              }
            })
            .state('home.trendings', {
              url: '/trendings',
              views:{
                'trending':{
                  templateUrl: '/states/trendings.html',
                  controller: 'trendingCtrl'
                }
              }
            })
            .state('home.chats', {
                url: '/chats',
                views:{
                  'chatroom':{
                    templateUrl: '/states/tab-chats.html',
                    controller: 'ChatsCtrl'
                  }
                }
              })
              .state('home.chatroom', {
                url: '/chats/:chatId',
                views:{
                  'chatroom':{
                    templateUrl: '/states/chat-detail.html',
                    controller: 'ChatDetailCtrl'
                  }
                }
              })
              .state('events', {
                url: '/events',
                templateUrl: '/states/avanEvents.html',
                controller: 'EventCtrl'
              })
              .state('learn', {
                url: '/learn',
                templateUrl: '/states/avanLearn.html',
                controller: 'LearnCtrl'
              })
              .state('room', {
                url: '/room',
                templateUrl: '/states/meetingRoom.html',
                controller: 'RoomCtrl'
              })
              .state('player', {
                url: '/player/:Id',
                templateUrl: '/states/videoPlayer.html',
                controller: 'VideoCtrl'
              })
        //$urlRouterProvider.otherwise('/');

        // date picker configuration
        var datePickerObj = {
              inputDate: new Date(),
              setLabel: 'Set',
              todayLabel: 'Today',
              closeLabel: 'Close',
              mondayFirst: false,
              weeksList: ["S", "M", "T", "W", "T", "F", "S"],
              monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
              templateType: 'popup',
              from: new Date(2012, 8, 1),
              to: new Date(2018, 8, 1),
              showTodayButton: true,
              dateFormat: 'dd MMMM yyyy',
              closeOnSelect: false,
              disableWeekdays: [6],
            };
            ionicDatePickerProvider.configDatePicker(datePickerObj);

        // Time picker configuration
        var timePickerObj = {
              inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
              format: 12,
              step: 15,
              setLabel: 'Set',
              closeLabel: 'Close'
            };
            ionicTimePickerProvider.configTimePicker(timePickerObj);

});