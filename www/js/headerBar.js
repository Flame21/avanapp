//
angular.module("avanapp.directives.headerbar",['ionic',
	'jett.ionic.filter.bar',
	"avanapp.directives.sideMenu"])
.directive('headerBar',function(){
	return{
		restrict: 'E',
		scope:{
			expanded:'=',
			onIconClick:'=',
			onSearch:'=',
			onCancel:'=',
			title:'=',
		},
		templateUrl:'template/HeaderBar.html',
		controller: function($scope,$ionicFilterBar) {
			//alert($scope.expanded);
			$scope.showFilterBar = function () {
			    var filterBarInstance = $ionicFilterBar.show({
			      cancelText: "<i class='ion-ios-close-outline'></i>",
			      update: function (filteredItems, filterText) {
			        //$scope.places = filteredItems;
			        //TODO: search users using some api
			      }
			    });
			  };
		}

	};
});